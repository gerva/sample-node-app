# sample node.js app
[![codebeat badge](https://codebeat.co/badges/734e6084-e7ae-4396-a64a-8ccd9ab8431a)](https://codebeat.co/projects/gitlab-com-gerva-sample-node-app-master)

welcome to sample app (with node.js)

## How to get started

The project lives in gitlab and assumes the application will be deployed on
a dokku enable host.
To setup a dokku service in the cloud, please follow the instructions [here](http://gitlab.com/gerva/dokku)
The above step is a prerequisite for having a working deployment.

### Gitlab setup

Assuming you have created your environments with the `dokku` automation:
Ensure your .gitlab-ci.yml points out the right dokku hosts.

Ensure you have set up a `SSH_PRIVATE_KEY` in your gitlab variables. This key
is the private part of the one deployed by ansible on dokku hosts.

For more information, please check `ssh key configuration for pushing code to dokku`
section [here](http://gitlab.com/gerva/dokku)


## Branching strategy

For a small size project, I will keep branching as simple as possible.
One main branch to track production (e.g: master), one development branch where
new features are added. Once features are stable, merge to the `production`
branch and tag the branch (`release-1.0.1`, `v3.4.1`...)

## Test stages

For this project I have added few tests stages:

- unittest
- integration test with selenium
- external code quality check

please note: unittest and integration tests are just placeholders (tests
always pass)

`unittest`: are added to test the code behaves as expected. It's not necessary to
cover all the code with unittest but it would be useful to have some, to help
refactoring. Unittests are executed before the application is deployed. If
unittest fail, the pipeline terminates.

`integration tests`: checks that, after the deployment, the website behaves
as expected.

`quality checks`: provide metrics on code, helps developers to improve code

## Versioning

In this specific case, a simple one page application, versioning can be simply
incremental: call version 1.0 the first one getting to production, add a +.1
for any following version.

# Environments

The ansible script is flexible enough to create as many environments are needed.
In this case, I opted for 2: `testing` and `production`


# Fan In/Fan out

Pipelines in this version are simple.
In this specific example, `production` and `testing` use the same pipeline,
except for few things: `production` does not execute the full `integration tests`
suite (`smoke tests` only).
Ideally you can plugin as many environment as needed eg: `testing -> staging`
and add different set of tests after `staging` deployment.

# Continuous Delivery or Continuous Deployment

If the code passes the tests, it gets deployed, and the integration tests are
triggered. Gitlab provides an easy way to rollback in case of issues.
Production deployment track master branch: code is reviewed by humans on merge
request (`master` is a protected branch). Once approved, code gets through
the same pipelines of build and test (maybe deployment to production can be
triggered manually)

# Release strategy

Following this pattern, `master` branch reflects `production`, having a new
version in production means merging code to `master` branch, pass tests and
deploy to `production`. I've left this operation triggered manually because,
sometime, production environments require a some business/higher manager
approvals.

# Limitations

- host names are hardcoded: they should become a variable that is updated, using the gitlab API when a new environment is created/updated
- automation lives in a separate repository as it will end up in the deployed version. Moreover the ansible automation is a generic tool to create dokku instances in AWS, and it has nothing to do with the final nodejs application
- there should be a better integration with the ansible tool and the creation of the application. The ansible automation can/should be part of the deploy stage (so we make sure the target environment exists and is well configured)
